package version

const VERSION = "1.0.0"

// Compulsory minimum version, Minimum downward compatibility to this version
func GetVersion() string {
	return "1.0.0"
}
