package proxy

import (
	"net"
	"net/http"
	"net/url"
	"sync"

	"ehang.io/nps/lib/cache"
	"ehang.io/nps/lib/common"
	"ehang.io/nps/lib/conn"
	"ehang.io/nps/lib/crypt"
	"ehang.io/nps/lib/file"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/pkg/errors"
)

type HttpsServer struct {
	httpServer
	listener         net.Listener
	httpsListenerMap sync.Map
}

func NewHttpsServer(l net.Listener, bridge NetBridge, useCache bool, cacheLen int) *HttpsServer {
	https := &HttpsServer{listener: l}
	https.bridge = bridge
	https.useCache = useCache
	if useCache {
		https.cache = cache.New(cacheLen)
	}
	return https
}

// start https server
func (https *HttpsServer) Start() error {
	// 根据https_just_proxy接收请求
	if b, err := beego.AppConfig.Bool("https_just_proxy"); err == nil && b {
		// https_just_proxy = true 内网处理https
		conn.Accept(https.listener, func(c net.Conn) {
			https.handleHttps(c)
		})
	} else {
		// https_just_proxy = false nps处理https

		// 读取默认ssl证书
		certFile := beego.AppConfig.String("https_default_cert_file")
		keyFile := beego.AppConfig.String("https_default_key_file")

		// 如果默认ssl证书存在
		if common.FileExists(certFile) && common.FileExists(keyFile) {
			// 创建一个https监听器
			l := NewHttpsListener(https.listener)

			// 使用默认ssl证书开启https服务器
			https.NewHttps(l, certFile, keyFile)

			// 添加到https监听器map中
			https.httpsListenerMap.Store("default", l)
		}

		// 开始接受请求
		conn.Accept(https.listener, func(c net.Conn) {
			// 从客户端的client hello消息读取主机信息
			serverName, rb := GetServerNameFromClientHello(c)

			// 如果客户端的client hello sni为空,默认主机统一为default
			if serverName == "" {
				serverName = "default"
			}

			// 如果https监听器已经包含此主机信息
			var l *HttpsListener
			if v, ok := https.httpsListenerMap.Load(serverName); ok {
				l = v.(*HttpsListener)
			} else {
				// 如果https监听器不包含此主机信息,通过主机信息创建http请求
				r := buildHttpsRequest(serverName)

				// 读取主机在nps配置文件
				if host, err := file.GetDb().GetInfoByHost(serverName, r); err != nil {
					// 主机未配置ssl证书直接关闭请求
					c.Close()
					logs.Notice("host does not exist, the url %s can't be parsed!,remote addr %s", serverName, c.RemoteAddr().String())
					return
				} else {
					// 如果主机的ssl证书文件不存在,则依然使用默认证书,默认证书也不存在则关闭请求
					if !common.FileExists(host.CertFilePath) || !common.FileExists(host.KeyFilePath) {
						// 使用默认证书的https监听器
						if v, ok := https.httpsListenerMap.Load("default"); ok {
							l = v.(*HttpsListener)
						} else {
							// 默认证书和主机证书都不存在则关闭请求
							c.Close()
							logs.Error("the key %s cert %s file is not exist", host.KeyFilePath, host.CertFilePath)
							return
						}
					} else {
						// 创建一个https监听器,使用主机的ssl证书
						l = NewHttpsListener(https.listener)
						// 创建https服务,nps处理https逻辑
						https.NewHttps(l, host.CertFilePath, host.KeyFilePath)
						https.httpsListenerMap.Store(serverName, l)
					}
				}
			}
			acceptConn := conn.NewConn(c)
			acceptConn.Rb = rb
			l.acceptConn <- acceptConn
		})
	}
	return nil
}

// close
// 关闭https服务
func (https *HttpsServer) Close() error {
	return https.listener.Close()
}

// 在创建https服务时,使用证书和密钥文件创建https服务
func (https *HttpsServer) NewHttps(l net.Listener, certFile string, keyFile string) {
	go func() {
		logs.Error(https.NewServer(0, "https").ServeTLS(l, certFile, keyFile))
	}()
}

// 内网处理https请求
func (https *HttpsServer) handleHttps(c net.Conn) {
	hostName, rb := GetServerNameFromClientHello(c)
	var targetAddr string
	r := buildHttpsRequest(hostName)
	var host *file.Host
	var err error
	if host, err = file.GetDb().GetInfoByHost(hostName, r); err != nil {
		c.Close()
		logs.Notice("the url %s can't be parsed!", hostName)
		return
	}
	if err := https.CheckFlowAndConnNum(host.Client); err != nil {
		logs.Warn("client id %d, host id %d, error %s, when https connection", host.Client.Id, host.Id, err.Error())
		c.Close()
		return
	}
	defer host.Client.AddConn()
	if err = https.auth(r, conn.NewConn(c), host.Client.Cnf.U, host.Client.Cnf.P); err != nil {
		logs.Warn("auth error", err, r.RemoteAddr)
		return
	}
	if targetAddr, err = host.Target.GetRandomTarget(); err != nil {
		logs.Warn(err.Error())
	}
	logs.Trace("new https connection,clientId %d,host %s,remote address %s", host.Client.Id, r.Host, c.RemoteAddr().String())
	https.DealClient(conn.NewConn(c), host.Client, targetAddr, rb, common.CONN_TCP, nil, host.Flow, host.Target.LocalProxy)
}

type HttpsListener struct {
	acceptConn     chan *conn.Conn
	parentListener net.Listener
}

// https listener
func NewHttpsListener(l net.Listener) *HttpsListener {
	return &HttpsListener{parentListener: l, acceptConn: make(chan *conn.Conn)}
}

// accept
func (httpsListener *HttpsListener) Accept() (net.Conn, error) {
	httpsConn := <-httpsListener.acceptConn
	if httpsConn == nil {
		return nil, errors.New("get connection error")
	}
	return httpsConn, nil
}

// close
func (httpsListener *HttpsListener) Close() error {
	return nil
}

// addr
func (httpsListener *HttpsListener) Addr() net.Addr {
	return httpsListener.parentListener.Addr()
}

// get server name from connection by read client hello bytes
func GetServerNameFromClientHello(c net.Conn) (string, []byte) {
	buf := make([]byte, 4096)
	data := make([]byte, 4096)
	n, err := c.Read(buf)
	if err != nil {
		return "", nil
	}
	if n < 42 {
		return "", nil
	}
	copy(data, buf[:n])
	clientHello := new(crypt.ClientHelloMsg)
	clientHello.Unmarshal(data[5:n])
	return clientHello.GetServerName(), buf[:n]
}

// build https request
func buildHttpsRequest(hostName string) *http.Request {
	r := new(http.Request)
	r.RequestURI = "/"
	r.URL = new(url.URL)
	r.URL.Scheme = "https"
	r.Host = hostName
	return r
}
